import React, { useState } from 'react';
import {MenuItems} from './Menuitem';
import './Navbar.css';
import * as FaIcons from "react-icons/fa";
import * as BiIcons from "react-icons/bi";
import * as BsIcons from "react-icons/bs";
import {Button} from '../Button/Button'

export default function Navbar() {


    const [state, setstate] = useState({clicked:false})


    
    function  handleClick(){
        setstate({clicked:!state.clicked})
    }
    

    return (
        <div>
            <nav className="NavbarItems">
                <h1 className="navbar-logo">React  <BiIcons.BiAtom  className=" fa-react"/> </h1>
                <div className="menu-icon"  onClick={  handleClick }  >
                            {state.clicked ? <BsIcons.BsX className="fa-close"/> : <FaIcons.FaBars className="fa-bars" />}

                        

                </div>
                <ul className={state.clicked ? 'nav-menu active' : 'nav-menu'}>
                        {MenuItems.map((item,index) => {
                          
                          return (

                                <li key={index}>
                                    <a className={item.cName} href={item.url}>
                                        {item.title}
                                    </a>
                                </li>   
                          )
                        })}
                </ul>
                <Button >Sign Up</Button>
            </nav>

        </div>
    )
}
